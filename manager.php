<?php
$host = "";  
$user = "";           
$pw = "";              
$database = "";       
//connect to database
$db = mysql_connect($host,$user,$pw)
        or die("Cannot connect to MySQL.");

mysql_select_db($database,$db)
        or die("Cannot connect to database.");

$view  = $_POST["v"];
if (isset($view))
{
viewproducts();
}

function viewproducts(){
$displayproducts = "SELECT * FROM products";
$dproducts = mysql_query($displayproducts);
while ($productsresults=mysql_fetch_object($dproducts))
{
echo $productsresults->name." - ".$productsresults->price."</br>";
}
}

//view customer purchases when button is clicked
$viewcustomers  = $_POST["vc"];
if (isset($viewcustomers))
{
viewcustomers();
viewpurchases();
}

//function to iterate over all customer results;
function viewcustomers(){
echo "<h2>Customers List</h2>";
$displaycustomers= "SELECT * FROM customers";
$dcustomers= mysql_query($displaycustomers);
while ($customersresults=mysql_fetch_object($dcustomers))
{
echo "ID: ".$customersresults->id." Name: ".$customersresults->name." Address: ".$customersresults->address." Phone: ".$customersresults->phone."</br>";
}
}

//function to view purchases
function viewpurchases(){
echo "<br>";
echo "<h2>Orders List</h2>";
$displayorders= "SELECT * FROM orders";
$dorders= mysql_query($displayorders);
while ($orderresults=mysql_fetch_object($dorders))
{
echo "Order ID: ".$orderresults->orderid." Customer ID: ".$orderresults->customerid." Name: ".$orderresults->name." Purchases: ".$orderresults->purchases.", Product ID: ".$orderresults->productid." Time: ".$orderresults->ordertime."</br>";
}
}

//retrieve product name and price from manager input
$pname = $_POST["name"];
$pprice = $_POST["price"];
if (isset($pname) && isset($pprice))
{
addproduct($pname,$pprice);
}

//add product to products table
function addproduct($name,$price){
$findproducts = "SELECT * FROM products WHERE name = '".$name."'";
$findprods = mysql_query($findproducts);
$product = mysql_fetch_object($findprods);
if ($product==NULL)
{
$addproduct = "INSERT INTO products (productid, name, price) VALUES ('', '".$name."',".$price.")";
$successadd = mysql_query($addproduct);
}

if ($successadd)
{
echo "Item successfully added.";
}
else
{
echo "Item not added.";
}
}

//retrieve update product information
$oldname = $_POST["oldname"];
$uname = $_POST["newname"];
$uprice = $_POST["newprice"];

//update product when all information are obtained
if (isset($oldname) && isset($uname) && isset($uprice))
{
updateproduct($oldname,$uname,$uprice);
}

//update product according to retreieved input
function updateproduct($oname, $nname, $nprice){
$updateproduct = "UPDATE products SET name='".$nname."', price=".$nprice." WHERE name='".$oname."'";
$successupdate = mysql_query($updateproduct);
if ($successupdate)
{
echo "Item successfully updated.";
}
else
{
echo "Item not updated.";
}
}

mysql_close($db); 
?>
<html>
<body>
<h2>Add Product</h2>
<form action='' method='POST'>
<label for='name'>Name</label>
<input type='text' name='name'>
<br>
<label for='price'>Price</label>
<input type='text' name='price'>
<br>
<input type='submit' value='Add'>
</form>
<h2>Update Product</h2>
<form action='' method='POST'>
<label for='oldname'>Old Name</label>
<input type='text' name='oldname'>
<br>
<label for='newname'>New Name</label>
<input type='text' name='newname'>
<br>
<label for='newprice'>New Price</label>
<input type='text' name='newprice'>
<br>
<input type='submit' value='Update'>
</form>
<br>
<h2>View Products</h2>
<form action='' method='POST'>
<button value='View' name='v'>View</button>
</form>
<h2>View Customers</h2>
<form action='' method='POST'>
<button value='View' name='vc'>View</button>
</form>
</body>
</html>
